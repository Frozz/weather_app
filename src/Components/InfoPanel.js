import React from 'react';
import './Components Styles/InfoPanel.css';


class InfoPanel extends React.Component {
  constructor() {
    super();

    this.state = {

      showDetailWeather: false,

    }
  }

  showDetails = () => {
    this.setState({
      showDetailWeather: true,
    })
  }

  hideDetails = () => {
    this.setState({
      showDetailWeather: false,
    })
  }

  render() {

    return (

      <div onMouseEnter={this.showDetails} onMouseLeave={this.hideDetails} className="infoWrapper">

        <span className="date">{this.props.data.applicable_date}</span>

        <h1>{this.props.cityName.charAt(0).toUpperCase() + this.props.cityName.slice(1)}</h1>

        <img alt={this.props.data.weather_state_name} src={`https://www.metaweather.com/static/img/weather/${this.props.data.weather_state_abbr}.svg`} />

        <span>{Math.round(this.props.data.the_temp) + " ℃"}</span>

        {this.state.showDetailWeather &&
          <div className="detailsWrapper">

            <p>Air pressure: {this.props.data.air_pressure + " 	(mbar)"}</p>
            <p>Date: {this.props.data.applicable_date}</p>
            <p>Humidity: {this.props.data.humidity + " (%)"}</p>
            <p>Max temp: {Math.round(this.props.data.max_temp) + " ℃"}</p>
            <p>Min temp: {Math.round(this.props.data.min_temp) + " ℃"}</p>
            <p>Current temp: {Math.round(this.props.data.the_temp) + " ℃"}</p>
            <p>Visibility: {Math.round(this.props.data.visibility) + " (miles)"}</p>
            <p>Weather state: {this.props.data.weather_state_name}</p>
            <p>Wind direction: {this.props.data.wind_direction_compass}</p>
            <p>Wind speed: {Math.round(this.props.data.wind_speed) + " (mph)"}</p>

          </div>}

      </div>
    )
  }
}

export default InfoPanel;