import React from 'react';
import './Components Styles/InputPanel.css';

const InputPanel = ( {weatherData, inputUpdate, value, geoWeatherData }) => {

  return (
    <div className="inputWrapper">
      <label htmlFor="searchInput">Search for location: </label>
      <input onChange={inputUpdate} type="text" value={value} />
      <button onClick={weatherData}>search</button>
      <button onClick={geoWeatherData}>my location</button>
    </div>
  )
}

export default InputPanel;