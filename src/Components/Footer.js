import React from 'react';
import './Components Styles/Footer.css';

const Footer = () => {

  return (
    <footer>
      <div className="footerWrapper">
        <span>Created by Metaweather API</span>
      </div>
    </footer>
  )
}

export default Footer;