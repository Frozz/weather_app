import React from 'react';
import './Components Styles/Header.css';

const Header = () => {

  return (
    <div className="headerWrapper">
      <div className="headerShadow">
        <h1>Weather App</h1>
      </div>
    </div>
  )
}

export default Header