import axios from 'axios';

export const getWeatherByCityName = location => {

  return axios.get(`https://www.metaweather.com/api/location/search/?query=${location}`).then(resp => {
    const woeid = getWoeid(resp);
    return axios.get(`https://www.metaweather.com/api/location/${woeid}`).then(resp => resp.data);
  });
}

export const getWeatherByLocation = (lat, lon) => {

  return axios.get(`https://www.metaweather.com/api/location/search/?lattlong=${lat},${lon}`).then(resp => {
    const { woeid } = resp.data[0];
    return axios.get(`https://www.metaweather.com/api/location/${woeid}`).then(resp => resp.data);
  })
}

const getWoeid = (resp) => {

  try {
    const { woeid } = resp.data[0];
    return woeid;

  } catch {
    alert('Location not found.')
  }
}



