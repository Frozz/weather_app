import React from 'react';
import { getWeatherByCityName } from './axios.js';
import { getWeatherByLocation } from './axios.js';
import Header from './Components/Header.js';
import InputPanel from './Components/InputPanel.js';
import InfoPanel from './Components/InfoPanel.js';
import Footer from './Components/Footer.js';
import './App.css';

class App extends React.Component {
  constructor() {
    super();

    this.state = {

      cityName: '',

      inputLocation: '',

      currentDay: '',
      day2: '',
      day3: '',
      day4: '',

    }
  }

  updateInputValue = (event) => {
    this.setState({
      inputLocation: event.target.value,
    })
  }

  getWeatherByLocation = () => {

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(location => {
        const { latitude, longitude } = location.coords;
        getWeatherByLocation(latitude, longitude).then(resp => {

          this.setState({

            currentDay: resp.consolidated_weather[0],
            day2: resp.consolidated_weather[1],
            day3: resp.consolidated_weather[2],
            day4: resp.consolidated_weather[3],

            cityName: '',

          })
        })
      })

    } else {

      alert('The function is not available in your browser.')
    }
  }

  getWeatherAndLocation = () => {
    const inputLocation = this.state.inputLocation;

    getWeatherByCityName(inputLocation).then((resp) => {

      this.setState({

        cityName: inputLocation,
        currentDay: resp.consolidated_weather[0],
        day2: resp.consolidated_weather[1],
        day3: resp.consolidated_weather[2],
        day4: resp.consolidated_weather[3],

        inputLocation: '',

      })  
    })
  }

  render() {

    return (
      <div>

        <Header />

        <InputPanel
          weatherData={this.getWeatherAndLocation}
          geoWeatherData={this.getWeatherByLocation}
          inputUpdate={this.updateInputValue}
          value={this.state.inputLocation} />

        <main>
          {this.state.currentDay &&
            <div className="mainWrapper">
              <InfoPanel
                data={this.state.currentDay}
                cityName={this.state.cityName} />
              <InfoPanel
                data={this.state.day2}
                cityName={this.state.cityName} />
              <InfoPanel
                data={this.state.day3}
                cityName={this.state.cityName} />
              <InfoPanel
                data={this.state.day4}
                cityName={this.state.cityName} />
            </div>}
        </main>

        <Footer />

      </div>
    )
  }
}

export default App;
